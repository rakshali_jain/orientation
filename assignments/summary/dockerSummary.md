## **Docker**
1. Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.
1. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels.
1. All containers are run by a single operating system kernel and therefore use fewer resources than virtual machines.

#### **Docker Commands**
A few Docker commands are:
1. docker –version
<!-- blank line -->
This command is used to get the currently installed version of docker

2. docker pull
<!-- blank line -->
Usage: docker pull <image name>
<!-- blank line -->
This command is used to pull images from the docker repository(hub.docker.com)

![ALT](https://storage.googleapis.com/static.configserverfirewall.com/images/docker/docker-pull-ubuntu.png)

3. docker run
<!-- blank line -->
Usage: docker run -it -d <image name>
<!-- blank line -->
This command is used to create a container from an image

![ALT](https://miro.medium.com/max/971/1*0HC7_ozbfrSQqcblCnWr_g.png)

4. docker ps
<!-- blank line -->
This command is used to list the running containers

![ALT](https://tecadmin.net/tutorial/wp-content/uploads/2017/09/docker-ps-command.png)

5. docker ps -a
<!-- blank line -->
This command is used to show all the running and exited containers

![ALT](https://miro.medium.com/max/1568/1*3KOEfJSCqlvUDHhGQ0MGog.png)

6. docker exec
<!-- blank line -->
Usage: docker exec -it <container id> bash
<!-- blank line -->
This command is used to access the running container

![ALT](https://phoenixnap.com/kb/wp-content/uploads/2019/10/get-access-to-docker-image.png)

7. docker stop
<!-- blank line -->
Usage: docker stop <container id>
<!-- blank line -->
This command stops a running container

8. docker kill
<!-- blank line -->
Usage: docker kill <container id>
<!-- blank line -->
This command kills the container by stopping its execution immediately. 

![ALT](https://miro.medium.com/max/1440/1*EXcTXK68nfcgJwR2UMq46g.png)

9. docker commit
<!-- blank line -->
Usage: docker commit <conatainer id> <username/imagename>
<!-- blank line -->
This command creates a new image of an edited container on the local system

![ALT](https://miro.medium.com/max/1566/1*IdUi9OdTM-_IFFd11rdt6Q.png)

10. docker login
<!-- blank line -->
This command is used to login to the docker hub repository

![ALT](https://miro.medium.com/max/1442/1*Ow3npJLdEW1J4296AI85LA.png)

