## **GIT**
1. Git was created by Linus Torvalds in 2005 for development of the Linux kernel, with other kernel developers contributing to its initial development.
1. Git is a distributed version-control system for tracking changes in source code during software development.
1. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows.

#### **Basic GIT commands**
1. git init – create a new local repository.
1. git clone – clone the repository on local system.
1. git add – adds changes in the working directory to the staging area.
1. git commit – saves the files from staging area into the local Git repository.
1. git push origin master – saves the files in local repo onto the remote repo.
1. git pull – fetch and merge changes on the remote server to your working directory.

#### **GIT Workflow**

![ALT](https://i2.wp.com/build5nines.com/wp-content/uploads/2018/01/GitHub-Flow.png?fit=3222%2C1111&ssl=1)

#### **Steps**
1. Initialize the git


![ALT](https://3.bp.blogspot.com/-UdSXZZEcGN4/V4Wwn-T-0gI/AAAAAAAAAEY/JGLE72AOSSoQvGZeMWOQesIPRjnVMcJewCLcB/w640/init.PNG)

2. Clone the Repository


![ALT](https://docs.microsoft.com/en-us/azure/devops/repos/git/media/move-git-repos-between-team-projects/moverepo-mirror-done.png?view=azure-devops)

3. Modify files in your working tree. Selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.
                   

4. Do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repository.
                    
5. Do a push, which takes the files as they are in the Local Git Repository and stores that snapshot permanently to your Remote Git Repository.


![ALT](https://miro.medium.com/max/491/1*O2VTwG_oGhmD67PcFwV5xQ.png)

